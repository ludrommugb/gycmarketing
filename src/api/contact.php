<?php

header('Content-Type: application/json');

$data = file_get_contents('php://input');
$data = json_decode($data);

if (!empty($data)) {
    require_once 'phpmailer/PHPMailerAutoload.php';

    $name = $data->name;
    $subject = $data->subject;
    $message = $data->message;

    $email_body_name = isset($name)
        ? "<label class='label'>Nombre: </label><span>$name</span><br>"
        : '';
    $email_body_subject = isset($subject)
        ? "<label class='label'>Asunto: </label><span>$subject</span><br>"
        : '';
    $email_body_message = isset($message)
        ? "<label class='label'>Mensaje: </label><span>$message</span>"
        : '';

    $email_body =
        "<html>
               <head>
                    <style>
                        *{
                            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
                            color: #1d1d1d;
                        }
                        .container {
                            font-size: 16px !important;
                            width: 80%;
                            margin: 0 auto;
                            border: 1px solid #dcdcdc;
                        }
                        h1 {
                            font-size: 18px !important;
                        }
                        .header {
                            text-align: center;
                            padding: 8px 0;
                            background-color: #00aac9;
                            font-size: 16px;
                            color: #fff;
                        }
                        .section {
                            margin: 8px 0;
                            padding: 12px;
                        }
                        .label {
                            font-size: 16px;
                            font-weight: bold;
                            color:#3f3d56;
                        }
                        .discalimer span{
                            font-size: 12px;
                            color: #5f6368;
                            font-style: italic;
                        }
                    </style>
                </head>
               <body>
                   <div class='container'>
                       <div class='personal-data'>
                           <div class='header'>
                               <h1>Contacto</h1>
                           </div>
                           <div class='section'>
                               $email_body_name
                               $email_body_subject
                               $email_body_message
                           </div>
                       </div>
                   </div>
               </body>
        </html>";

    // Email info
    $email = new PHPMailer();
    //$email->SMTPDebug = SMTP::DEBUG_SERVER;
    //$email->SMTPDebug = 2; //Alternative to above constant
    $email->isSMTP();
    $email->Debugoutput = 'html';
    $email->Host = 'grupo-bedoya.com';
    $email->Port = 465;
    $email->SMTPSecure = 'ssl';
    $email->SMTPAuth = true;

    $email_from = "contacto@gyc-marketing.com";
    $email_pass = "gycmarketing2020!";
    $email_name = "GyC Marketing y Publicidad";

    $subject = "Formulario de Contacto - GyC Marketing y Publicidad";
    $email_me = "manuelgbmediagroup@gmail.com";
    $email_to = "luisace17@gyc-marketing.com";

    $email->Username = $email_from;
    $email->Password = $email_pass;

    // Set who the message is to be sent from
    $email->setFrom($email_from, $email_name);

    $email->isHTML(true);
    $email->Subject = $subject;
    $email->AltBody = "txtAltBody";
    $email->CharSet = 'UTF-8';
    //$email->addAddress($email_me);
    $email->addAddress($email_to);
    $email->Body = $email_body;

    try {
        $sent = $email->Send();
        echo json_encode(['success' => $sent, 'message' => 'Request sent with no errors']);
    } catch (phpmailerException $e) {
        echo json_encode(['success' => false, 'message' => 'Error sending the request']);
    }
} else {
    echo json_encode(['success' => false, 'message' => 'You have no access to this resource']);
}